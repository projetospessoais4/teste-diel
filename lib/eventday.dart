import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_mask/easy_mask.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../standard/pesquisa.dart';

import '../standard/design.dart';
import '../standard/layout.dart';
import 'standard/objetos.dart';

class EventsDay extends StatefulWidget {
  final DateTime day;

  const EventsDay({Key? key, required this.day}) : super(key: key);

  @override
  _EventsDayState createState() => _EventsDayState();
}

class _EventsDayState extends State<EventsDay> {
  TextEditingController TituloController = TextEditingController();
  TextEditingController DescricaoController = TextEditingController();
  TextEditingController HorarioController = TextEditingController();
  TextEditingController DuracaoController = TextEditingController();

  List<String> resultado = [];
  List<String> ordemEvento = [];
  List<EventosObj> EventosLista = [];
  bool newEvent = false;
  bool editEvent = false;
  int indexEdit = 0;

  QuerySnapshot? eventsquery;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: null,
      body: SingleChildScrollView(
        child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(children: [
          const SizedBox(height: 35),
          Layout()
              .titulo("Eventos dia: ${Pesquisa().formatDateTime(widget.day)}"),
          const SizedBox(height: 5),
          newEvent ? _novoEvento() : Container(),
          const SizedBox(height: 5),
          _ListaDeEventos(),
          newEvent
              ? Container()
              : Align(
                  alignment: Alignment.center,
                  child: TextButton(
                    onPressed: () {
                      setState(() {
                        newEvent = true;
                      });
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Cores.corbotao,
                        ),
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: Layout().texto('Adicionar evento +', 16,
                            FontWeight.normal, Colors.black,
                            align: TextAlign.center,
                            textDecoration: TextDecoration.underline)),
                  )),
        ]),
      ),
    ));
  }

  Widget _ListaDeEventos() {
    return StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection(ColecaoNomes.EVENTS)
            .where('data', isEqualTo: widget.day)
            .orderBy('horario', descending: false)
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            print(snapshot.error);
            return const Text(
                'Isto é um erro. Por gentileza, contate o suporte.');
          }
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return const Text("Sem conexão");
            case ConnectionState.waiting:
              return const Center(child: CircularProgressIndicator());
            default:
              return (snapshot.requireData.size >= 1)
                  ? ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: snapshot.requireData.size,
                  itemBuilder: (context, index) {
                    if (resultado.length < snapshot.requireData.size) {
                      resultado.add('');
                      ordemEvento.add(snapshot.data!.docs[index].id);
                    }
                    EventosObj eventoObj = EventosObj();
                    eventoObj = eventoObj.EventosFromDoc(
                            snapshot.data!.docs[index]);
                    return CardEvento(eventoObj, index);
                  })
                  : Center(
                  child: Layout().texto(
                      'Não Eventos marcados para esse dia',
                      16.0,
                      FontWeight.normal,
                      Colors.black));
          }
        });
  }

  Widget CardEvento(EventosObj evento, index) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
        child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                        onPressed: () {
                          excluir(index);
                        },
                        icon: const Icon(Icons.delete_forever_outlined)),
                    IconButton(
                        onPressed: () {
                          TituloController.text = evento.titulo;
                          DescricaoController.text = evento.descricao;
                          HorarioController.text = evento.horario;
                          DuracaoController.text = evento.duracao;
                          setState(() {
                            newEvent = true;
                            editEvent = true;
                            indexEdit = index;
                          });
                        },
                        icon: const Icon(Icons.edit_outlined))
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Layout().texto(
                        evento.titulo, 20, FontWeight.bold, Colors.black),
                    Layout().texto(evento.horario, 18, FontWeight.normal,
                        Colors.black),
                  ],
                ),
                Layout().texto(
                    evento.descricao, 14, FontWeight.normal, Colors.black),
                Layout().texto(evento.duracao, 14, FontWeight.normal,
                    Colors.black),
                Container(
                  width: MediaQuery.of(context).size.width * 0.2,
            ),
            const SizedBox(height: 5),
            const SizedBox(height: 5),
            const Divider(thickness: 1.0, color: Colors.black),
            const SizedBox(height: 5),
          ],
        ));
  }

  Widget _novoEvento() {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
        child: Column(
          children: [
            Layout().caixadetexto(1, 1, TextInputType.text, TituloController,
                "Titulo do Evento", TextCapitalization.none, Colors.grey,
                labelColor: Colors.black54),
            Layout().caixadetexto(1, 1, TextInputType.text, DescricaoController,
                "Descriçao do Evento", TextCapitalization.none, Colors.grey,
                labelColor: Colors.black54),
            Layout().caixadetextoFormatada(
                1,
                1,
                TextInputType.number,
                HorarioController,
                "Horario de inicio do Evento",
                TextCapitalization.none,
                Colors.grey,
                labelColor: Colors.black54,
                format: TextInputMask(mask: '99:99')),
            Layout().caixadetextoFormatada(
                1,
                1,
                TextInputType.number,
                DuracaoController,
                "Duração do Evento",
                TextCapitalization.none,
                Colors.grey,
                labelColor: Colors.black54,
                format: TextInputMask(mask: '99:99')),
            SizedBox(height: 20),
            Row(
              children: [
                TextButton(
                  onPressed: () {
                    if(editEvent)
                      editar();
                    else
                    salvar();
                  },
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Cores.corbotao,
                      ),
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: Layout().texto(
                          'Salvar', 16, FontWeight.normal, Colors.black,
                          align: TextAlign.center,
                          textDecoration: TextDecoration.underline)),
                ),
                TextButton(
                  onPressed: () {
                    setState(() {
                      newEvent = false;
                      editEvent = false;
                      indexEdit = 0;
                    });
                  },
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Cores.corbotao,
                      ),
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: Layout().texto(
                          'Cancelar', 16, FontWeight.normal, Colors.black,
                          align: TextAlign.center,
                          textDecoration: TextDecoration.underline)),
                )
              ],
            ),
          ],
        ));
  }

  Future<void> salvar() async {
    if (TituloController.text != "" &&
        DescricaoController.text != "" &&
        HorarioController.text != "" &&
        DuracaoController.text != "") {
      Map<String, dynamic> mapEvento = Map<String, dynamic>();
      mapEvento['titulo'] = TituloController.text;
      mapEvento['descricao'] = DescricaoController.text;
      mapEvento['horario'] = HorarioController.text;
      mapEvento['duracao'] = DuracaoController.text;
      mapEvento['data'] = widget.day;

      FirebaseFirestore.instance.collection(ColecaoNomes.EVENTS).add(mapEvento);

      TituloController.text='';
      DescricaoController.text='';
      HorarioController.text='';
      DuracaoController.text='';

      setState(() {
        newEvent = false;
      });
    }
    else{
      Layout().dialog1botao(context, "Campos Obrigatorios", "todos os campos são obrigatorios");
    }
  }

  Future<void> excluir(index) async {

    FirebaseFirestore.instance.collection(ColecaoNomes.EVENTS).doc(ordemEvento[index]).delete();

      setState(() {
        newEvent = false;
      });
  }

  Future<void> editar() async {
    if (TituloController.text != "" &&
        DescricaoController.text != "" &&
        HorarioController.text != "" &&
        DuracaoController.text != "") {
      Map<String, dynamic> mapEvento = Map<String, dynamic>();
      mapEvento['titulo'] = TituloController.text;
      mapEvento['descricao'] = DescricaoController.text;
      mapEvento['horario'] = HorarioController.text;
      mapEvento['duracao'] = DuracaoController.text;
      mapEvento['data'] = widget.day;

      FirebaseFirestore.instance.collection(ColecaoNomes.EVENTS).doc(ordemEvento[indexEdit]).update(mapEvento);

      TituloController.text='';
      DescricaoController.text='';
      HorarioController.text='';
      DuracaoController.text='';

      setState(() {
        newEvent = false;
        editEvent = false;
        indexEdit = 0;
      });
    }
    else{
      Layout().dialog1botao(context, "Campos Obrigatorios", "todos os campos são obrigatorios");
    }
  }
}
