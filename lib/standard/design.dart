import 'package:flutter/material.dart';

const double widthOfContainerDashboard = 600.0;
const double heightOfContainerDashboard = 400.0;

class Cores {
  static Color? corfundo = Colors.blueGrey[50];
  static Color corprincipal = const Color.fromRGBO(4, 170, 192, 1.0);
  static Color corbotao = const Color.fromRGBO(255, 143, 0, 1.0);
  static Color verdeecomp = const Color.fromRGBO(0, 136, 114, 1.0);
}

class ColecaoNomes {
  static const String EVENTS = "Events";
}

class NotificacaoTipo {
  static const String TESTE = 'teste';
}