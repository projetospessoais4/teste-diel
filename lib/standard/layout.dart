import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../main.dart';
import 'design.dart';
import 'pesquisa.dart';

class Layout {
  Widget titulo(texto,
      {bool smallSize = false,
      Color color = Colors.black,
      EdgeInsets padding = const EdgeInsets.all(8.0),
      aling}) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        (texto.toString().isNotEmpty) ? texto : "Selecione",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: smallSize ? 15 : 18.0,
            fontFamily: "Sans",
            letterSpacing: 1.5,
            color: texto.toString().isNotEmpty ? color : Colors.grey),
        textAlign: aling,
      ),
    );
  }

  Widget texto(texto, size, fontWeight, color,
      {TextAlign? align,
      int? maxLines,
      TextOverflow? overflow,
      TextDecoration? textDecoration,
      double? height}) {
    return Text(
      texto,
      overflow: overflow,
      style: TextStyle(
        height: height,
        fontFamily: "Kanit",
        color: color,
        fontSize: size?.toDouble(),
        fontWeight: fontWeight,
        decoration: textDecoration,
      ),
      textAlign: align,
      maxLines: maxLines,
    );
  }

  Widget caixadetexto(
    min,
    max,
    textinputtype,
    controller,
    placeholder,
    capitalization,
    colortexto, {
    bool obs: false,
    labelColor,
    labelFontWeight,
    padding = const EdgeInsets.only(left: 20.0, right: 20.0, top: 14.0),
    maxLength,
    onChanged,
  }) {
    return Padding(
      padding: padding,
      child: TextField(
        onChanged: onChanged,
        minLines: min,
        maxLines: max,
        maxLength: maxLength,
        keyboardType: textinputtype,
        autocorrect: true,
        enableSuggestions: true,
        controller: controller,
        decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: const Color.fromRGBO(4, 170, 192, 1.0), width: 1.5),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 1.5),
          ),
          labelText: placeholder,
          labelStyle: TextStyle(
              letterSpacing: 1.0,
              fontSize: 14.0,
              fontWeight: labelFontWeight,
              color: labelColor ?? Colors.black),
        ),
        textCapitalization: capitalization,
        autofocus: false,
        obscureText: obs,
        style: TextStyle(color: colortexto, fontSize: 18.0),
      ),
    );
  }

  Widget caixadetextoFormatada(
    min,
    max,
    textinputtype,
    controller,
    placeholder,
    capitalization,
    colortexto, {
    bool obs: false,
    labelColor,
    labelFontWeight,
    padding = const EdgeInsets.only(left: 20.0, right: 20.0, top: 14.0),
    maxLength,
    onChanged,
    format,
  }) {
    return Padding(
      padding: padding,
      child: TextField(
        inputFormatters: [format],
        onChanged: onChanged,
        minLines: min,
        maxLines: max,
        maxLength: maxLength,
        keyboardType: textinputtype,
        autocorrect: true,
        enableSuggestions: true,
        controller: controller,
        decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: const Color.fromRGBO(4, 170, 192, 1.0), width: 1.5),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 1.5),
          ),
          labelText: placeholder,
          labelStyle: TextStyle(
              letterSpacing: 1.0,
              fontSize: 14.0,
              fontWeight: labelFontWeight,
              color: labelColor ?? Colors.black),
        ),
        textCapitalization: capitalization,
        autofocus: false,
        obscureText: obs,
        style: TextStyle(color: colortexto, fontSize: 18.0),
      ),
    );
  }

  dialogConfirmacaoDeletar(Function deleteFunction, BuildContext context,
      deleteDialogTitle, telaDeSucessoTitle,
      {textoBotaoConfirmar = 'Sim, excluir'}) {
    showDialog(
        builder: (context) {
          return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.08),
            child: AlertDialog(
                contentPadding: EdgeInsets.all(0),
                titlePadding: EdgeInsets.all(0),
                title: Container(
                  color: const Color.fromRGBO(4, 170, 192, 1.0),
                  child: Center(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Layout().texto(
                        deleteDialogTitle, 16, FontWeight.normal, Colors.white,
                        align: TextAlign.center),
                  )),
                ),
                content: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: const Color.fromRGBO(4, 170, 192, 1.0),
                          width: 3)),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 20, bottom: 20),
                        child: Container(
                            height: 40,
                            child: Image.asset('images/delete_icon.png')),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          color: const Color.fromRGBO(4, 170, 192, 1.0),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 25),
                            child: Layout().texto(
                                'Não', 16, FontWeight.normal, Colors.white),
                          ),
                        ),
                      ),
                      TextButton(
                          onPressed: () {
                            deleteFunction();
                            if (telaDeSucessoTitle != null) {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          Home()));
                            }
                          },
                          child: Layout().texto(
                              textoBotaoConfirmar,
                              16,
                              FontWeight.normal,
                              const Color.fromRGBO(4, 170, 192, 1.0),
                              align: TextAlign.center,
                              textDecoration: TextDecoration.underline))
                    ],
                  ),
                )),
          );
        },
        context: context);
  }


  dialog1botao(context, titulo, texto,
      {destino, destinopush, barrierDismissible = true}) {
    showCupertinoDialog(
        barrierDismissible: barrierDismissible,
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(titulo),
            content: Text(texto),
            actions: <Widget>[
              CupertinoDialogAction(
                isDefaultAction: true,
                child: Text(
                  "OK",
                  style:
                      TextStyle(color: const Color.fromRGBO(4, 170, 192, 1.0)),
                ),
                onPressed: () {
                  if (destino == null && destinopush == null) {
                    Navigator.pop(context);
                  } else if (destinopush != null) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => destinopush));
                  } else {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => destino),
                        (Route<dynamic> route) => false);
                  }
                },
              ),
            ],
          );
        });
  }

  dialog2botoesFuncao(context, String titulo, String texto,
      {required String textoBotaoDireito,
      required Function funcaoBotaoDireito,
      required String textoBotaoEsquerdo,
      required Function funcaoBotaoEsquerdo}) {
    showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(titulo),
            content: Text(texto),
            actions: <Widget>[
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text(
                    textoBotaoEsquerdo,
                    style: TextStyle(
                        color: const Color.fromRGBO(4, 170, 192, 1.0)),
                  ),
                  onPressed: null /*funcaoBotaoEsquerdo*/),
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text(
                    textoBotaoDireito,
                    style: TextStyle(
                        color: const Color.fromRGBO(4, 170, 192, 1.0)),
                  ),
                  onPressed: null /*funcaoBotaoDireito*/),
            ],
          );
        });
  }
}
