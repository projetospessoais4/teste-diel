import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EventosObj{
  String titulo = '';
  String descricao = '';
  String horario = '';
  String duracao = '';

  EventosFromDoc(DocumentSnapshot doc){
    titulo = doc["titulo"] ?? "";
    descricao = doc["descricao"];
    horario = doc["horario"];
    duracao = doc["duracao"];

    return this;
  }
}