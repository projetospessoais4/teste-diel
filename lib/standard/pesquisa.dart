import 'dart:async';

import 'package:cpf_cnpj_validator/cpf_validator.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../main.dart';
import 'design.dart';
//import 'package:firebase/firebase.dart' as fb;

class Pesquisa {

  String replaceforpush(String texto) {
    String newtexto = texto
        .replaceAll(RegExp(' '), '')
        .replaceAll(RegExp('ç'), 'c')
        .replaceAll(RegExp('/'), '')
        .replaceAll(RegExp('-'), '')
        .replaceAll(RegExp('ê'), 'e')
        .replaceAll(RegExp('á'), 'a')
        .replaceAll(RegExp('º'), '')
        .replaceAll(RegExp('ã'), 'a')
        .replaceAll(RegExp('â'), 'a');
    return newtexto;
  }

  isFieldValueNull(Map<String, dynamic> map, field) {
    if (map[field] == null) {
      return true;
    }
    return false;
  }

  dynamic verifyField(
      DocumentSnapshot documentSnapshot, String field, placeholder,
      {String? subField1, String? subField2}) {
    dynamic result = placeholder;
    if (documentSnapshot != null && documentSnapshot[field] != null) {
      if (field != null) {
        result = documentSnapshot[field];
      } else {
        return result;
      }

      if (subField1 != null) {
        if (isFieldValueNull(documentSnapshot[field], subField1)) {
          return placeholder;
        }
        result = documentSnapshot[field][subField1];
      } else {
        return result;
      }

      if (subField2 != null) {
        if (isFieldValueNull(
            documentSnapshot[field][subField1], subField2)) {
          return placeholder;
        }
        result = documentSnapshot[field][subField1][subField2];
      } else {
        return result;
      }
    }
    return result;
  }

  dynamic verifyMapField(Map<String, dynamic> map, String field, placeholder,
      {String? subField1, String? subField2}) {
    dynamic result = placeholder;
    if (map != null && map[field] != null) {
      if (field != null) {
        if (isFieldValueNull(map, field)) {
          return placeholder;
        }
        result = map[field];
      } else {
        return result;
      }

      if (subField1 != null) {
        if (isFieldValueNull(map[field], subField1)) {
          return placeholder;
        }
        result = map[field][subField1];
      } else {
        return result;
      }

      if (subField2 != null) {
        if (isFieldValueNull(map[field][subField1], subField2)) {
          return placeholder;
        }
        result = map[field][subField1][subField2];
      } else {
        return result;
      }
    }
    return result;
  }

  String formatCurrency(numero, {bool hasSymbol = true}) {
    NumberFormat formatCurrency = NumberFormat.simpleCurrency(
        locale: 'pt-BR', name: hasSymbol ? null : '');
    return formatCurrency.format(numero);
  }

  enviarnotificacaotoken(List token, String mensagem, {String screen = ''}) {
    token.forEach((element) {
      Map<String, Object> notification = Map();
      notification['token'] = element;
      notification['title'] = '7MinutesPay';
      notification['mensagem'] = mensagem;
      notification['screen'] = screen;
  });
  }

  salvarfirebase(collection, map, file, fileweb) async {
    if (file != null) {
      String nomearquivo =
          '$collection/${map['userid']}/${DateTime.now().toIso8601String()}.jpg';
    } else if (fileweb != null) {
    } else {
      FirebaseFirestore.instance.collection(collection).add(map);
    }
  }

  salvarfirebasepdf(collection, map, file, fileweb) async {
    if (file != null) {
      String nomearquivo =
          '$collection/${map['unidade']}/${DateTime.now().toIso8601String()}.pdf';
    } else if (fileweb != null) {
    } else {
      FirebaseFirestore.instance.collection(collection).add(map);
    }
  }


  abrirsite(link) async {
    if (await canLaunch(link)) {
      await launch(link);
    } else {
      throw 'Não conseguimos abrir $link';
    }
  }

  salvar(map, collection, context) {
    map['data'] = hoje();
    map['createdAt'] = DateTime.now().toIso8601String();
    map['datacomparar'] = DateTime.now();
    FirebaseFirestore.instance.collection(collection).add(map);
  }

  atualizar(map, collection, DocumentSnapshot doc, context) {
    FirebaseFirestore.instance
        .collection(collection)
        .doc(doc.id)
        .update(map);
  }

  substituirpara(destino, context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => destino),
    );
  }

  irParaHome(context) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => Home(
            )),
            (Route<dynamic> route) => false);
  }

  irpara(destino, BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => destino),
    );
  }

  void pushReplacement(BuildContext context, Widget page) {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute<void>(builder: (_) => page),
    );
  }

  bool saboudom() {
    DateTime now = DateTime.now();
    String formattedDate2 = DateFormat('EEEE').format(now);
    if (formattedDate2 == "Sunday") {
      return true;
    }
    return false;
  }

  String hoje() {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('dd/MM/yyyy').format(now);
    return formattedDate;
  }

  String hojesembarra() {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('ddMMyyyy').format(now);
    return formattedDate;
  }

  String getData1(DateTime data) {
    String formattedDate = DateFormat('dd/MM/yyyy').format(data);
    return formattedDate;
  }

  String formatData({int? year, int? month, int? day}) {
    DateTime data = DateTime(year!, month!, day!);
    String formattedDate = DateFormat('dd/MM/yyyy').format(data);
    return formattedDate;
  }

  String formatDateTime(DateTime date,
      {String format = 'dd/MM/yyyy',
        String horasFormat = 'HH:mm',
        bool? showHours}) {
    String formattedDate = DateFormat(format).format(date);
    String formattedHoras = DateFormat(horasFormat).format(date);
    if (showHours != null && showHours == true) {
      return formattedDate + ' ' + formattedHoras;
    } else {
      return formattedDate;
    }
  }

  String formatHora({int? hour, int? minute}) {
    DateTime data = DateTime(0, 1, 1, hour!, minute!);
    String formattedDate = DateFormat('HH:mm').format(data);
    return formattedDate;
  }

  String getDataeHora() {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('dd/MM/yy HH:mm').format(now);
    return formattedDate;
  }

  String getHora() {
    DateTime now = DateTime.now();
    String formattedDate1 = DateFormat('HH:mm').format(now);
    return formattedDate1;
  }

/*  getMes() {
    DateTime now = DateTime.now();
    String formattedDate =
        DateFormat('MM').format(now) + ' de ' + DateFormat('yyyy').format(now);
    return formattedDate;
  }*/

  getMesTelaInicial() {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('MMyyyy').format(now);
    return formattedDate;
  }

  getAno() {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy').format(now);
    return formattedDate;
  }

  bool validarCPF(String cpf) {
    if (cpf != null && cpf.isNotEmpty) {
      if (CPFValidator.isValid(cpf.replaceAll('.', '').replaceAll('-', ''))) {
        return true;
      }
    }
    return false;
  }

  Future<double> atualizaAnuncioValorResgate(anuncioDoc) async {
    double preco = anuncioDoc['precovenda'];
    double valorResgate = preco * 0.9;
    await anuncioDoc.reference.updateData({'valorResgate': valorResgate});
    return valorResgate;
  }
}
